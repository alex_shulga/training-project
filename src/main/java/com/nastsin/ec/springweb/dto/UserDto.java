package com.nastsin.ec.springweb.dto;

import lombok.Data;

@Data
public class UserDto {

    private String id;
    private String name;
    private String addressId;
    private String street;
    private String city;

    public String getId() {
        return id;
    }
    public String getCity() {
        return this.city;
    }

    public String getStreet() {
        return this.street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public void setCity(String city) {
        this.city = city;
    }
}