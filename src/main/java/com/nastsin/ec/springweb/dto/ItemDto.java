package com.nastsin.ec.springweb.dto;

import lombok.Data;

@Data
public class ItemDto {

    private String id;
    private String title;
    private String userId;

    public String getId() {
        return this.id;
    }
}