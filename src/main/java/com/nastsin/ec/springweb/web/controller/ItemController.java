package com.nastsin.ec.springweb.web.controller;

import com.nastsin.ec.springweb.dto.ItemDto;
import com.nastsin.ec.springweb.entity.Item;
import com.nastsin.ec.springweb.exception.NotFoundException;
import com.nastsin.ec.springweb.mapper.ItemMapper;
import com.nastsin.ec.springweb.service.DefaultItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/items")
public class ItemController {

    private final DefaultItemService itemService;
    private final ItemMapper mapper;

    @Autowired
    public ItemController(DefaultItemService itemService, ItemMapper mapper) {
        this.itemService = itemService;
        this.mapper = mapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<ItemDto> findItem(@PathVariable("id") Long id) throws NotFoundException {
        Item item = itemService.getItem(id);
        ItemDto itemDto = mapper.toDto(item);
        return new ResponseEntity<>(itemDto, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<ItemDto>> getAllItems() {
        List<Item> items = itemService.getItems();
        List<ItemDto> itemsDto = new ArrayList<>();
        for (Item item : items) {
            itemsDto.add(mapper.toDto(item));
        }
        return new ResponseEntity<>(itemsDto, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ItemDto> saveItem(@RequestBody ItemDto itemDto) {
        Item item = mapper.toEntity(itemDto);
        Item savedItem = itemService.saveItem(item);
        ItemDto savedItemDto = mapper.toDto(savedItem);
        return new ResponseEntity<>(savedItemDto, HttpStatus.OK);
    }

    @PutMapping
    public ResponseEntity<ItemDto> updateItem(@RequestBody ItemDto itemDto) throws NotFoundException {
        Item item = mapper.toEntity(itemDto);
        itemService.getItem(item.getId());
        Item savedItem = itemService.saveItem(item);
        ItemDto savedItemDto = mapper.toDto(savedItem);
        return new ResponseEntity<>(savedItemDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ItemDto> deleteUser(@PathVariable("id") Long id) throws NotFoundException {
        this.itemService.deleteItem(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
