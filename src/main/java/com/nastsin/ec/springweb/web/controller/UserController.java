package com.nastsin.ec.springweb.web.controller;

import com.nastsin.ec.springweb.dto.UserDto;
import com.nastsin.ec.springweb.entity.User;
import com.nastsin.ec.springweb.exception.NotFoundException;
import com.nastsin.ec.springweb.mapper.UserMapper;
import com.nastsin.ec.springweb.service.DefaultUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/v1/users")
public class UserController {

    private final DefaultUserService userService;
    private final UserMapper mapper;

    @Autowired
    public UserController(DefaultUserService userService, UserMapper mapper) {
        this.userService = userService;
        this.mapper = mapper;
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserDto> findUser(@PathVariable("id") Long id) {
        User user = userService.getUser(id);
        System.out.println(user);
        UserDto userDto = mapper.toDto(user);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<UserDto>> getAllUsers() {
        List<User> users = userService.getUsers();
        List<UserDto> usersDto = new ArrayList<>();
        for (User user : users) {
            usersDto.add(mapper.toDto(user));
        }
        return new ResponseEntity<>(usersDto, HttpStatus.OK);
    }

    @PostMapping()
    public ResponseEntity<UserDto> saveUser(@RequestBody UserDto userDto) {
        User user = mapper.toEntity(userDto);
        if (!addressExistenceCheck(user)) {
            User savedUser = userService.saveUser(user);
            UserDto savedUserDto = mapper.toDto(savedUser);
            return new ResponseEntity<>(savedUserDto, HttpStatus.OK);
        }
        else {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDto userDto) throws NotFoundException {
        User user = mapper.toEntity(userDto);
        userService.getUser(user.getId());
        User savedUser = userService.saveUser(user);
        UserDto savedUserDto = mapper.toDto(savedUser);
        return new ResponseEntity<>(savedUserDto, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserDto> deleteUser(@PathVariable("id") Long id) throws NotFoundException {
        userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    private boolean addressExistenceCheck(User user) {
        List<User> users = userService.getUsers();
        for (User userToCheck : users) {
            if (userToCheck.getAddress().getId().equals( user.getAddress().getId())){
                return true;
            }
        }
        return false;
    }
}
