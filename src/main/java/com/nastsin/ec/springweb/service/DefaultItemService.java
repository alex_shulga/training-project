package com.nastsin.ec.springweb.service;

import com.nastsin.ec.springweb.entity.Item;
import com.nastsin.ec.springweb.exception.NotFoundException;
import com.nastsin.ec.springweb.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DefaultItemService implements ItemService {

    private final ItemRepository itemRepository;

    @Autowired
    public DefaultItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @Override
    public Item getItem(Long id) throws NotFoundException {
        Optional<Item> item = itemRepository.findById(id);
        if (item.isPresent()) {
            return item.get();
        } else {
            throw new NotFoundException("Item with id: " + id + " was not found");
        }
    }

    @Override
    public Item saveItem(Item item) {
        return itemRepository.save(item);
    }

    @Override
    public List<Item> getItems() {
        return itemRepository.findAll();
    }

    @Override
    public void deleteItem(Long id){
        itemRepository.deleteById(id);
    }
}
