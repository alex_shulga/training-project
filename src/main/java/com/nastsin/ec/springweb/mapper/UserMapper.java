package com.nastsin.ec.springweb.mapper;

import com.nastsin.ec.springweb.dto.UserDto;
import com.nastsin.ec.springweb.entity.User;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class UserMapper {

    private final ModelMapper mapper;

    @Autowired
    public UserMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public UserDto toDto(User entity) {
        UserDto userDto = Objects.isNull(entity) ? null : mapper.map(entity, UserDto.class);
        if(userDto != null && entity.getAddress() != null){
            userDto.setStreet(entity.getAddress().getStreet());
            userDto.setCity(entity.getAddress().getCity());
        }
        return userDto;
    }

    public User toEntity(UserDto addressDto) {
        User user = Objects.isNull(addressDto) ? null : mapper.map(addressDto, User.class);
        if(user != null){
            user.getAddress().setStreet(addressDto.getStreet());
            user.getAddress().setCity(addressDto.getCity());
            user.getAddress().setUser(user);
        }
        return user;
    }
}
