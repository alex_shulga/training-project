package com.nastsin.ec.springweb.mapper;

import com.nastsin.ec.springweb.dto.AddressDto;
import com.nastsin.ec.springweb.dto.ItemDto;
import com.nastsin.ec.springweb.entity.Address;
import com.nastsin.ec.springweb.entity.Item;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class ItemMapper {

    private final ModelMapper mapper;

    @Autowired
    public ItemMapper(ModelMapper mapper) {
        this.mapper = mapper;
    }

    public Item toEntity(ItemDto itemDto) {
        return Objects.isNull(itemDto) ? null : mapper.map(itemDto, Item.class);
    }

    public ItemDto toDto(Item entity) {
        return Objects.isNull(entity) ? null : mapper.map(entity, ItemDto.class);
    }
}
